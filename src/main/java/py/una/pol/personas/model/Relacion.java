package py.una.pol.personas.model;

public class Relacion {
	private Long id ;
	private Long id_asignatura ;
	private Long id_persona ;
	public Relacion() {

	}
	public Relacion(Long id, Long id_asignatura, Long id_persona) {
		this.id = id;
		this.id_asignatura = id_asignatura;
		this.id_persona = id_persona;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId_asignatura() {
		return id_asignatura;
	}
	public void setId_asignatura(Long id_asignatura) {
		this.id_asignatura = id_asignatura;
	}
	public Long getId_persona() {
		return id_persona;
	}
	public void setId_persona(Long id_persona) {
		this.id_persona = id_persona;
	}

	
	
}
