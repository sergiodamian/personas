/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.service.AsignaturaService;

/**
 * JAX-RS Example
 * <p/>
 * Esta clase produce un servicio RESTful para leer y escribir contenido de
 * asignaturas
 */
@Path("/asignaturas")
@RequestScoped
public class AsignaturaRESTService {

	@Inject
	private Logger log;

	@Inject
	AsignaturaService asignaturaService;

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<Asignatura> listar() {
		return asignaturaService.seleccionar();
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Asignatura obtenerPorId(@PathParam("id") long id) {
		Asignatura p = asignaturaService.seleccionarPorId(id);
		if (p == null) {
			log.info("obtenerPorId " + id + " no encontrado.");
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		log.info("obtenerPorId " + id + " encontrada: " + p.getNombre());
		return p;
	}

	@GET
	@Path("/id")
	@Produces(MediaType.APPLICATION_JSON)
	public Asignatura obtenerPorIdQuery(@QueryParam("id") long id) {
		Asignatura p = asignaturaService.seleccionarPorId(id);
		if (p == null) {
			log.info("obtenerPorId " + id + " no encontrado.");
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		log.info("obtenerPorId " + id + " encontrada: " + p.getNombre());
		return p;
	}

	/**
	 * Creates a new member from the values provided. Performs validation, and will
	 * return a JAX-RS response with either 200 ok, or with a map of fields, and
	 * related errors.
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response crear(Asignatura p) {

		Response.ResponseBuilder builder = null;

		try {
			asignaturaService.crear(p);
			// Create an "ok" response

			// builder = Response.ok();
			builder = Response.status(201).entity("Asignatura creada exitosamente");

		} catch (SQLException e) {
			// Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		return builder.build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response modificar(Asignatura p) {

		Response.ResponseBuilder builder = null;

		try {
			asignaturaService.actualizar(p);
			// Create an "ok" response

			// builder = Response.ok();
			builder = Response.status(201).entity("Asignatura modificada exitosamente");

		} catch (SQLException e) {
			// Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		return builder.build();
	}

	@DELETE
	@Path("/{id}")
	public Response borrar(@PathParam("id") long id) {
		Response.ResponseBuilder builder = null;
		try {

			if (asignaturaService.seleccionarPorId(id) == null) {

				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
			} else {
				asignaturaService.borrar(id);
				builder = Response.status(202).entity("Asignatura borrada exitosamente.");
			}

		} catch (SQLException e) {
			// Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();
	}

	@GET
	@Path("/asociar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response asociar(@QueryParam("id") long id, @QueryParam("cedula") long cedula) {

		Response.ResponseBuilder builder = null;

		try {
			asignaturaService.asociar(cedula, id);

			builder = Response.status(201).entity("Asignatura asociada exitosamente");

		} catch (SQLException e) {
			// Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		return builder.build();
	}

	@GET
	@Path("/desasociar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response desasociar(@QueryParam("id") long id, @QueryParam("cedula") long cedula) {

		Response.ResponseBuilder builder = null;

		try {
			asignaturaService.desasociar(cedula, id);

			builder = Response.status(201).entity("Asignatura desasociada exitosamente");

		} catch (SQLException e) {
			// Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		return builder.build();
	}

	@GET
	@Path("/listaAsignaturas")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listaAsignaturas(@QueryParam("cedula") long cedula) {

		Response.ResponseBuilder builder = null;

		List<Asignatura> lista;

		try {
			lista = asignaturaService.listarAsignaturas(cedula);
			if (lista == null) {
				builder = Response.status(201).entity("No existe asignaturas asociada a la persona");
			} else {
				builder = Response.status(201).entity(lista);
			}
			
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		return builder.build();
	}

	@GET
	@Path("/listaPersonas")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarPersonas(@QueryParam("id") long id) {

		Response.ResponseBuilder builder = null;

		List<Persona> lista;

		try {
			lista = asignaturaService.listarPersonas(id);
			if (lista == null) {
				builder = Response.status(201).entity("No existe peronas asociadas a la asignatura");
			} else {
				builder = Response.status(201).entity(lista);
			}
			
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		return builder.build();
	}
}
