Laboratorio de WebServices

**Elementos a utilizar**
* JavaEE, JDK 1.8
* IDE Eclipse, elegir la opción Eclipse for Java EE
* IDE Eclipse, instalar Jboss Tools (Menú: Help -> Eclipse MarketPlace. Buscar "Jboss Tools"). Este paso solo es necesario para crear un proyecto nuevo.
* Instalar Jboss tools 
* Instalar SOAP-UI
* Instalar PostMan para Google Chrome
* Instalar el motor de base de datos Postgresql

**Descargar el proyecto**

Via SSH:
    
    git clone git@gitlab.com:sergiodamian/personas.git
    
Via HTTPS:
    
    git clone https://gitlab.com/sergiodamian/personas.git
    
Via transferencia de archivos por parte del Profesor

    
**Importar proyecto**
 * Abrir el IDE Eclipse
 * Menú File -> Import -> Existing Maven Project
 * Dentro de los archivos descargados, especificar la carpeta "sd\lab-ws\servidor\personas"
 * Finalizar
 * El IDE comenzará a importar las librerías de Maven
 
**Base de datos**
 * Crear una base de datos.
 * Configurar los datos de configuración y acceso en la clase "Bd.java".

```sql
CREATE TABLE persona
(
    cedula integer NOT NULL,
    nombre character varying(1000),
    apellido character varying(1000),
    CONSTRAINT pk_cedula PRIMARY KEY (cedula)
)
WITH (
   OIDS=FALSE
 );
```

```sql
CREATE TABLE asignatura
(
    id integer NOT NULL,
    nombre character varying(1000) COLLATE pg_catalog."default",
    CONSTRAINT pk_id PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.asignatura
    OWNER to postgres;
```

```sql
CREATE SEQUENCE relacion_id_seq
    INCREMENT 1
    START 9
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE relacion_id_seq
    OWNER TO postgres;
```

```sql
CREATE TABLE relacion
(
    id integer NOT NULL DEFAULT nextval('relacion_id_seq'::regclass),
    id_asignatura integer NOT NULL,
    id_persona integer NOT NULL,
    CONSTRAINT pk_relacion PRIMARY KEY (id),
    CONSTRAINT fk_asignatura FOREIGN KEY (id_asignatura)
        REFERENCES public.asignatura (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_persona FOREIGN KEY (id_persona)
        REFERENCES public.persona (cedula) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
```

**Deployar en Servidor**
 * Desde el IDE Eclipse, configurar el servidor de aplicaciones Wildfly10 (Verificar la guía de clase anterior sobre el laboratorio de JavaRMI)
 * Deploy del proyecto "personas" en el servidor Wildfly


**Verificación con herramientas: POSTMAN / SOAP-UI / Browser**
 * PUT http://localhost:8080/personas/rest/asignaturas
 * GET http://localhost:8080/personas/rest/asignaturas
 * DELETE http://localhost:8080/personas/rest/asignaturas/1
 * GET http://localhost:8080/personas/rest/asignaturas/asociar?id=1&cedula=40
 * GET http://localhost:8080/personas/rest/asignaturas/desasociar?id=1&cedula=40
 * GET http://localhost:8080/personas/rest/asignaturas/listaAsignaturas?cedula=35
 * GET http://localhost:8080/personas/rest/asignaturas/listaPersonas?id=2